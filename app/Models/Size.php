<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Size
 *
 */
class Size extends Model
{
    protected $table = 'sizes';

    protected $fillable = ['name', 'user_id', 'width', 'height', 'size'];

    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
