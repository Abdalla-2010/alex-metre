<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Category extends Model
{
    public function getTranslation($field = '', $lang = false)
    {
        $lang = $lang == false ? App::getLocale() : $lang;
        $category_translation = $this->hasMany(CategoryTranslation::class)->where('lang', $lang)->first();
        return $category_translation != null ? $category_translation->$field : $this->$field;
    }

    public function category_translations()
    {
        return $this->hasMany(CategoryTranslation::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function classified_products()
    {
        return $this->hasMany(CustomerProduct::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('categories');
    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function allTrans($categories = null)
    {
        $categories = $categories == null ? $this->all() : $categories;
        $categoriesTrans = CategoryTranslation::where("lang", routeLang())->get();
        $categories = $categories->map(function ($category) use ($categoriesTrans) {
            $categoryTrans = $categoriesTrans->where("category_id", $category->id)->first();
            if ($categoryTrans == null) return $category;
            $category['name'] = $categoryTrans->name;
            return $category;
        });
        return $categories;
    }
}
