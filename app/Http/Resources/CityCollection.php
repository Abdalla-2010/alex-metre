<?php

namespace App\Http\Resources;

use App\Country;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CityCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'id' => $data->id,
                    'country' => Country::find($data->country_id)->name,
                    'name' => $data->name,
                    'cost' => (string)$data->cost
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
