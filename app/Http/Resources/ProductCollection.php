<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'id' => (integer) $data->id,
                    'name' => $data->name,
                    'photos' => $this->convertPhotos(explode(',', $data->photos)),
                    'thumbnail_image' => api_asset($data->thumbnail_img),
                    'base_price' => (double)homeBasePrice($data->id),
                    'base_discounted_price' => (double)homeDiscountedBasePrice($data->id),
                    'todays_deal' => (integer)$data->todays_deal,
                    'featured' => (integer)$data->featured,
                    'unit' => $data->unit,
                    'discount' => (double)$data->discount,
                    'discount_type' => $data->discount_type,
                    'custom_size' => $data->custom_size,
                    'is_design' => $data->is_design,
                    'custom_size_price' => $data->custom_size_price,
                    'published' => $data->published,
                    'rating' => (double)$data->rating,
                    'sales' => (integer)$data->num_of_sale,
                    'links' => [
                        'details' => route('products.show', $data->id),
                        'reviews' => route('api.reviews.index', $data->id),
                        'related' => route('products.related', $data->id),
                        'top_from_seller' => route('products.topFromSeller', $data->id)
                    ]
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }

    protected function convertPhotos($data){
        $result = array();
        foreach ($data as $key => $item) {
            array_push($result, api_asset($item));
        }
        return $result;
    }
}
