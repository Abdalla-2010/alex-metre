<?php

namespace App\Http\Resources;

use App\Models\Size;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CartCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'id' => $data->id,
                    'product' => [
                        'id' => $data->product->id,
                        'name' => $data->product->name,
                        'image' => api_asset($data->product->thumbnail_img)
                    ],
                    'variation' => $data->variation,
                    'size_id' => $data->size_id,
                    'size_name' => $data->size_id ? Size::find($data->size_id)->name : null,
                    'price' => (double)$data->price,
                    'tax' => (double)$data->tax,
                    'shipping_cost' => (double)$data->shipping_cost,
                    'quantity' => (integer)$data->quantity,
                    'date' => $data->created_at->format('Y-m-d H:i:s')
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
