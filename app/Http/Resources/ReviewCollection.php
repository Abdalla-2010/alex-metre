<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ReviewCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'user' => [
                        'name' => $data->user->name
                    ],
                    'rating' => $data->rating,
                    'comment' => $data->comment,
                    'time' => Carbon::parse(
                        $data->created_at
                    )->toDateTimeString()
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
