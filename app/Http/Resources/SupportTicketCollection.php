<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SupportTicketCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'id' => (integer)$data->id,
                    'code' => $data->code,
                    'subject' => $data->subject,
                    'details' => $data->details,
                    'files' => $this->getFiles($data->files),
                    'status' => $data->status,
                    'viewed' => $data->viewed,
                    'client_viewed' => $data->client_viewed,
                    'ticketreplies' => $data->ticketreplies->map(function ($reply) {
                        return [
                            'id' => $reply->id,
                            'ticket_id' => $reply->ticket_id,
                            'reply' => $reply->reply,
                            'files' => $this->getFiles($reply->files),
                        ];
                    }),
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }

    public function getFiles($files)
    {
        if (!$files) return null;
        $links = [];
        foreach ((array)explode(",", $files) as $file) {
            $links[] = api_asset($file);
        }
        return $links;
    }
}
