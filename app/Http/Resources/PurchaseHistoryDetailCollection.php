<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PurchaseHistoryDetailCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                $all = [
                    'variation' => $data->variation,
                    'price' => $data->price,
                    'tax' => $data->tax,
                    'shipping_cost' => $data->shipping_cost,
                    'size_id' => $data->size_id,
                    'coupon_discount' => $data->coupon_discount,
                    'quantity' => $data->quantity,
                    'payment_status' => $data->payment_status,
                    'delivery_status' => $data->delivery_status
                ];
                if($data->product){
                    $all['product'] = $data->product->name;
                    $all['thumbnail_image'] = api_asset($data->product->thumbnail_img);
                }
                return $all;
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
