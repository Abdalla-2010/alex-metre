<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProfileCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'id' => $data->id,
                    'name' => $data->name,
                    'user_type' => $data->user_type,
                    'email' => $data->email,
                    'avatar' => $data->avatar,
                    'avatar_original' => api_asset($data->avatar_original),
                    'address' => $data->address,
                    'city' => $data->city,
                    'country' => $data->country,
                    'postal_code' => $data->postal_code,
                    'phone' => $data->phone,
                    'addresses' => $data->addresses->map(function ($add) {
                        return [
                            "id" => $add->id,
                            "address" => $add->address,
                            "country" => $add->country,
                            "city" => $add->city,
                            "postal_code" => $add->postal_code,
                            "phone" => $add->phone,
                            "set_default" => $add->set_default,
                        ];
                    }),
                    'sizes' => $data->sizes->map(function ($size) {
                        return [
                            "id" => $size->id,
                            "name" => $size->name,
                            "height" => $size->height,
                            "width" => $size->width,
                            "size" => $size->size,
                        ];
                    }),
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
