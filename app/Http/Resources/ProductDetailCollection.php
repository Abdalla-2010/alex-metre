<?php

namespace App\Http\Resources;

use App\Color;
use App\Models\Attribute;
use App\Models\Review;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductDetailCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'id' => (integer)$data->id,
                    'name' => $data->name,
                    'added_by' => $data->added_by,
                    'user' => [
                        'id' => $data->user->id,
                        'name' => $data->user->name,
                        'email' => $data->user->email,
                        'avatar' => $data->user->avatar,
                        'avatar_original' => api_asset($data->user->avatar_original),
                        'shop_name' => $data->added_by == 'admin' ? '' : $data->user->shop->name,
                        'shop_logo' => $data->added_by == 'admin' ? '' : uploaded_asset($data->user->shop->logo),
                        'shop_link' => $data->added_by == 'admin' ? '' : route('shops.info', $data->user->shop->id)
                    ],
                    'category' => $data->category ? [
                        'name' => $data->category->name,
                        'banner' => api_asset($data->category->banner),
                        'icon' => $data->category->icon,
                        'links' => [
                            'products' => route('api.products.category', $data->category_id),
                            'sub_categories' => route('subCategories.index', $data->category_id)
                        ]
                    ] : null,
                    'brand' => [
                        'name' => $data->brand != null ? $data->brand->name : null,
                        'logo' => $data->brand != null ? api_asset($data->brand->logo) : null,
                        'links' => [
                            'products' => $data->brand != null ? route('api.products.brand', $data->brand_id) : null
                        ]
                    ],
                    'photos' => $this->convertPhotos(explode(',', $data->photos)),
                    'thumbnail_image' => api_asset($data->thumbnail_img),
                    'tags' => explode(',', $data->tags),
                    'price_lower' => (double)explode('-', homeDiscountedPrice($data->id))[0],
                    'price_higher' => (double)explode('-', homeDiscountedPrice($data->id))[1],
                    'choice_options' => $this->convertToChoiceOptions(json_decode($data->choice_options)),
                    'colors' => $this->handleColors(json_decode($data->colors)),
                    'variations' => json_decode($data->variations),
                    'todays_deal' => (integer)$data->todays_deal,
                    'featured' => (integer)$data->featured,
                    'current_stock' => $this->handleStocks($data->stocks),
//                    'current_stock' => (integer)$data->current_stock,
                    'unit' => $data->unit,
                    'discount' => (double)$data->discount,
                    'discount_type' => $data->discount_type,
                    'custom_size' => $data->custom_size,
                    'is_design' => $data->is_design,
                    'custom_size_price' => $data->custom_size_price,
                    'tax' => (double)$data->tax,
                    'product_link' => route('product', $data->slug),
                    'tax_type' => $data->tax_type,
                    'shipping_type' => $data->shipping_type,
                    'shipping_cost' => (double)$data->shipping_cost,
                    'number_of_sales' => (integer)$data->num_of_sale,
                    'rating' => (double)$data->rating,
                    'rating_count' => (integer)Review::where(['product_id' => $data->id])->count(),
                    'description' => strip_tags($data->description),
                    'links' => [
                        'reviews' => route('api.reviews.index', $data->id),
                        'related' => route('products.related', $data->id)
                    ]
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }

    protected function convertToChoiceOptions($data)
    {
        $result = array();
        foreach ($data as $key => $choice) {
            $item['name'] = $choice->attribute_id;
            $item['title'] = Attribute::find($choice->attribute_id)->name;
            $item['options'] = $choice->values;
            array_push($result, $item);
        }
        return $result;
    }

    public function handleStocks($stocks)
    {
        $stocks->makeHidden(["created_at", "updated_at", "product_id", "id", "price", "sku"]);
        $one = false;
        foreach ($stocks as $stock) {
            $stock['variant'] == null && $one = $stock['qty'];
        }
        if ($one != false) {
            return $one;
        }
        return $stocks;
    }

    public function handleColors($colors)
    {
        $result = [];
        foreach ($colors as $color) {
            $colorD = Color::where("code", $color)->first();
            if ($colorD) {
                $result[$colorD->name] = $colorD->code;
            }
        }
        return $result;
    }

    protected function convertPhotos($data)
    {
        $result = array();
        foreach ($data as $key => $item) {
            array_push($result, api_asset($item));
        }
        return $result;
    }
}
