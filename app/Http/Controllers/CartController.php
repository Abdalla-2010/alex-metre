<?php

namespace App\Http\Controllers;

use App\Category;
use App\Color;
use App\Models\Cart;
use App\Product;
use App\SubSubCategory;
use Cookie;
use Illuminate\Http\Request;
use Session;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();
        $carts = getCarts();
        return view('frontend.view_cart', compact('categories', 'carts'));
    }

    private function getCarts()
    {
        if (!auth()->user()) return;
        $cartsWithoutPrices = Cart::where("user_id", auth()->user()->id)->get();
        $carts = [];
        foreach ($cartsWithoutPrices as $item) {
            $product = Product::where("id", $item->product_id)->first();
            $price = $product->unit_price;
            $flash_deals = \App\FlashDeal::where('status', 1)->get();
            $inFlashDeal = false;
            foreach ($flash_deals as $flash_deal) {
                if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $item['product_id']->id)->first() != null) {
                    $flash_deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $item['product_id'])->first();
                    if ($flash_deal_product->discount_type == 'percent') {
                        $price -= ($price * $flash_deal_product->discount) / 100;
                    } elseif ($flash_deal_product->discount_type == 'amount') {
                        $price -= $flash_deal_product->discount;
                    }
                    $inFlashDeal = true;
                    break;
                }
            }
            if (!$inFlashDeal) {
                if ($product->discount_type == 'percent') {
                    $price -= ($price * $product->discount) / 100;
                } elseif ($product->discount_type == 'amount') {
                    $price -= $product->discount;
                }
            }

            if ($product->tax_type == 'percent') {
                $tax = ($price * $product->tax) / 100;
            } elseif ($product->tax_type == 'amount') {
                $tax = $product->tax;
            }
            $item['price'] = $price;
            $carts[] = $item;
        }
        return $carts;
    }

    public function showCartModal(Request $request)
    {
        $product = Product::find($request->id);
        return view('frontend.partials.addToCart', compact('product'));
    }

    public function checkIfProdcutHasCustomSize(Request $request)
    {
        $product = Product::find($request->id);
        if ($product) {
            if ($product->custom_size) {
                return response()->json(["hasCustomSize" => 1, 'path' => route("product", $product->slug)]);
            } else {
                return response()->json(["hasCustomSize" => 0]);
            }
        }
    }

    public function updateNavCart(Request $request)
    {
        $carts = getCarts();
        return view('frontend.partials.cart', ['carts' => $carts, 'countCarts' => count($carts)]);
    }

    public function addToCart(Request $request)
    {
        $product = Product::find($request->id);
        $data = array();
        $data['id'] = $product->id;
        $data['owner_id'] = $product->user_id;
        $str = '';
        $price = 0;
        $tax = 0;

        if ($product->digital != 1 && $request->quantity < $product->min_qty) {
            return array('status' => 0, 'view' => view('frontend.partials.minQtyNotSatisfied', [
                'min_qty' => $product->min_qty
            ])->render());
        }

        //check the color enabled or disabled for the product
        if ($request->has('color')) {
            $str = Color::where('code', $request['color'])->first()->name;
        }
        if ($product->digital != 1) {
            foreach (json_decode(Product::find($request->id)->choice_options) as $key => $choice) {
                if ($str != null) {
                    $str .= '-' . str_replace(' ', '', $request['attribute_id_' . $choice->attribute_id]);
                } else {
                    $str .= str_replace(' ', '', $request['attribute_id_' . $choice->attribute_id]);
                }
            }
        }
        $size_attr = \App\Attribute::where("name", "Size")->get()->first();
        if ($size_attr == null) {
            $name = "custom_size";
        } else {
            $name = "attribute_id_" . $size_attr->id;
        }
        $inputName = "custom_size_";
        if ($product->custom_size && str_contains($request[$name], $inputName)) {
            if (str_contains($request[$name], $inputName)) {
                if ($request[$name] == $inputName) {
                    return array('status' => 0, 'view' => view('frontend.partials.mustChooseSize')->render());
                }
                $data['size_id'] = (int)$request[$name][strlen($inputName)];
                $price = $product->unit_price + $product->custom_size_price;
                $data['variant'] = "";
            }
        } else {
            $data['variant'] = $str;
            if ($str != null && $product->variant_product) {
                $product_stock = $product->stocks->where('variant', $str)->first();
                $price = $product_stock->price;
                $quantity = $product_stock->qty;

                if ($quantity < $request['quantity']) {
                    return array('status' => 0, 'view' => view('frontend.partials.outOfStockCart')->render());
                }
            } else {
                $price = $product->unit_price;
            }
        }
        $data['variant'] = $str;
        $flash_deals = \App\FlashDeal::where('status', 1)->get();
        $inFlashDeal = false;
        foreach ($flash_deals as $flash_deal) {
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first() != null) {
                $flash_deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first();
                if ($flash_deal_product->discount_type == 'percent') {
                    $price -= ($price * $flash_deal_product->discount) / 100;
                } elseif ($flash_deal_product->discount_type == 'amount') {
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }
        if (!$inFlashDeal) {
            if ($product->discount_type == 'percent') {
                $price -= ($price * $product->discount) / 100;
            } elseif ($product->discount_type == 'amount') {
                $price -= $product->discount;
            }
        }

        if ($product->tax_type == 'percent') {
            $tax = ($price * $product->tax) / 100;
        } elseif ($product->tax_type == 'amount') {
            $tax = $product->tax;
        }

        $data['quantity'] = $request['quantity'];
        $data['price'] = $price;
        $data['tax'] = $tax;
        $data['shipping'] = 0;
        $data['product_referral_code'] = null;
        $data['digital'] = $product->digital;

        if ($request['quantity'] == null) {
            $data['quantity'] = 1;
        }

        if (Cookie::has('referred_product_id') && Cookie::get('referred_product_id') == $product->id) {
            $data['product_referral_code'] = Cookie::get('product_referral_code');
        }

        if (auth()->check()) {
            $data2 = $data;
            $data2['product_id'] = $data2['id'];
            $data2['user_id'] = auth()->user()->id;
            $data2['variation'] = $data2['variant'];
            $data2['shipping_cost'] = $data2['shipping'];
            unset($data2['id']);
            unset($data2['owner_id']);
            unset($data2['variant']);
            unset($data2['shipping']);
            unset($data2['product_referral_code']);
            unset($data2['digital']);
            unset($data2['price']);
            $foundInCart = false;
            $result = null;
            if (isset($data['size_id']) && isset($data['size_id']) != null) {
                $oldCart = Cart::where(['size_id' => $data2['size_id'], 'variation' => $data2['variation'], 'user_id' => $data2['user_id'], 'product_id' => $data2['product_id']])->first();
                $result = $oldCart;
                if ($oldCart) {
                    $oldCart->quantity = $oldCart->quantity + $data2['quantity'];
                    $oldCart->save();
                } else {
                    $result = Cart::create($data2);
                }
//                    Cart::updateOrCreate(['size_id' => $data2['size_id'], 'variation' => $data2['variation'], 'user_id' => $data2['user_id'], 'product_id' => $data2['product_id']], ['quantity' => DB::raw('quantity + ' . $data['quantity'])]);
            } else {
                foreach (Cart::where("user_id", userID())->get() as $item) {
                    if ($item['product_id'] == $request->id && $item['variation'] == $data2["variation"]) {
                        $product_stock = $product->stocks->where('variant', $str)->first();
                        $quantity = $product_stock->qty;
                        if ($quantity < $item['quantity'] + $request['quantity']) {
                            return array('status' => 0, 'view' => view('frontend.partials.outOfStockCart')->render());
                        } else {
                            $foundInCart = true;
                            $item['quantity'] += $request['quantity'];
                        }
                        $result = Cart::where(["product_id" => $data2['product_id'], 'user_id' => userID(), "size_id" => null, "variation" => $data2["variation"]])
                            ->update(['quantity' => $item['quantity']]);
                    }
                }
                if (!$foundInCart) {
                    $result = Cart::create($data2);
                }
            }
        } else {
            if ($request->session()->has('cart')) {
                $foundInCart = false;
                $cart = collect();
                foreach ($request->session()->get('cart') as $key => $cartItem) {
                    if ($cartItem['id'] == $request->id) {
//                    if($cartItem['variant'] == $str && $str != null){
                        if ($cartItem['variant'] == $str) {
                            $product_stock = $product->stocks->where('variant', $str)->first();
                            $quantity = $product_stock->qty;

                            if ($quantity < $cartItem['quantity'] + $request['quantity']) {
                                return array('status' => 0, 'view' => view('frontend.partials.outOfStockCart')->render());
                            } else {
                                $foundInCart = true;
                                $cartItem['quantity'] += $request['quantity'];
                            }
                        }
                    }
                    $cart->push($cartItem);
                }

                if (!$foundInCart) {
                    $cart->push($data);
                }
                $request->session()->put('cart', $cart);
            } else {
                $cart = collect([$data]);
                $request->session()->put('cart', $cart);
            }
        }
        return array('status' => 1, 'view' => view('frontend.partials.addedToCart', compact('product', 'data', 'result', 'data2'))->render());
    }

    public function addToCartWithTshirt(Request $request)
    {
        $design = Product::find($request->product_id);
        $tshirt = Product::find($request->tshirt_id);

        $data = array();
        $data['id'] = $tshirt->id;
        $data['owner_id'] = $tshirt->user_id;
        $str = '';
        $tax = 0;

        $data['variant'] = "";

        if ($str != null && $tshirt->variant_product) {
            $product_stock = $tshirt->stocks->where('variant', $str)->first();
            $price = $product_stock->price;
            $quantity = $product_stock->qty;

            if ($quantity < $request['quantity']) {
                return array('status' => 0, 'view' => view('frontend.partials.outOfStockCart')->render());
            }
        } else {
            $price = $tshirt->unit_price;
        }

        $flash_deals = \App\FlashDeal::where('status', 1)->get();
        $inFlashDeal = false;
        foreach ($flash_deals as $flash_deal) {
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $tshirt->id)->first() != null) {
                $flash_deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $tshirt->id)->first();
                if ($flash_deal_product->discount_type == 'percent') {
                    $price -= ($price * $flash_deal_product->discount) / 100;
                } elseif ($flash_deal_product->discount_type == 'amount') {
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }
        if (!$inFlashDeal) {
            if ($tshirt->discount_type == 'percent') {
                $price -= ($price * $tshirt->discount) / 100;
            } elseif ($tshirt->discount_type == 'amount') {
                $price -= $tshirt->discount;
            }
        }

        if ($tshirt->tax_type == 'percent') {
            $tax = ($price * $tshirt->tax) / 100;
        } elseif ($tshirt->tax_type == 'amount') {
            $tax = $tshirt->tax;
        }

        $data['quantity'] = 1;
        $data['price'] = $price;
        $data['tax'] = $tax;
        $data['shipping'] = 0;
        $data['product_referral_code'] = null;
        $data['digital'] = $tshirt->digital;
        $data['design_id'] = $design->id;

        if ($request['quantity'] == null) {
            $data['quantity'] = 1;
        }

        if (Cookie::has('referred_product_id') && Cookie::get('referred_product_id') == $tshirt->id) {
            $data['product_referral_code'] = Cookie::get('product_referral_code');
        }

        if (!is_null(auth()->user())) {
            $data2 = $data;
            $data2['product_id'] = $data2['id'];
            $data2['user_id'] = auth()->user()->id;
            $data2['variation'] = $data2['variant'];
            $data2['shipping_cost'] = $data2['shipping'];
            $data2['design_id'] = $design->id;
            unset($data2['id']);
            unset($data2['owner_id']);
            unset($data2['variant']);
            unset($data2['shipping']);
            unset($data2['product_referral_code']);
            unset($data2['digital']);
            unset($data2['price']);

            $foundInCart = false;
            if (!$foundInCart) {
                Cart::create($data2);
            }
        } else {
            if ($request->session()->has('cart')) {
                $foundInCart = false;
                $cart = collect();

                foreach ($request->session()->get('cart') as $key => $cartItem) {
                    if ($cartItem['id'] == $request->id) {
                        if ($cartItem['variant'] == $str) {
                            $product_stock = $tshirt->stocks->where('variant', $str)->first();
                            $quantity = $product_stock->qty;

                            if ($quantity < $cartItem['quantity'] + $request['quantity']) {
                                return array('status' => 0, 'view' => view('frontend.partials.outOfStockCart')->render());
                            } else {
                                $foundInCart = true;
                                $cartItem['quantity'] += $request['quantity'];
                            }
                        }
                    }
                    $cart->push($cartItem);
                }

                if (!$foundInCart) {
                    $cart->push($data);
                }
                $request->session()->put('cart', $cart);
            } else {
                $cart = collect([$data]);
                $request->session()->put('cart', $cart);
            }
        }
        $buynow = $request->session()->pull("buynow", 0);
        return array('status' => 1, 'buynow' => $buynow, 'view' => view('frontend.partials.addedToCart', compact('tshirt', 'data', 'design'))->render());
    }

    //removes from Cart
    public function removeFromCart(Request $request)
    {
        if (!is_null(auth()->user())) {
            if (Cart::where("id", $request->key)->first()) {
                Cart::where("id", $request->key)->delete();
            }
        } else {
            if ($request->session()->has('cart')) {
                $cart = $request->session()->get('cart', collect([]));
                $cart->forget($request->key);
                $request->session()->put('cart', $cart);
            }
        }
        $carts = getCarts();

        return view('frontend.partials.cart_details', ["carts" => $carts]);
    }

    public function updateQuantity(Request $request)
    {
        if (!isNotLogin()) {
            $cartID = $request->key;
            $currentQuantity = $request->quantity;
            $cart = Cart::where('id', $cartID)->first();
            $product = Product::where("id", $cart->product_id)->first();
            if ($cart['variation'] != null && $product->variant_product) {
                if (str_contains($cart['variation'], "custom_size_")) {
                    $cart['quantity'] = $currentQuantity;
                } else {
                    $product_stock = $product->stocks->where('variant', $cart['variation'])->first();
                    $quantity = $product_stock->qty;
                    if ($quantity >= $currentQuantity) {
                        if ($currentQuantity >= $product->min_qty) {
                            $cart['quantity'] = $currentQuantity;
                        }
                    }
                }
            } elseif ($product->current_stock >= $currentQuantity) {
                if ($currentQuantity >= $product->min_qty) {
                    $cart['quantity'] = $currentQuantity;
                }
            }
            Cart::where('id', $cartID)->update($cart->toArray());
        } else {
            $cart = $request->session()->get('cart', collect([]));
            $cart = $cart->map(function ($object, $key) use ($request) {
                if ($key == $request->key) {
                    $product = \App\Product::find($object['id']);
                    if ($object['variant'] != null && $product->variant_product) {
                        $product_stock = $product->stocks->where('variant', $object['variant'])->first();
                        $quantity = $product_stock->qty;
                        if ($quantity >= $request->quantity) {
                            if ($request->quantity >= $product->min_qty) {
                                $object['quantity'] = $request->quantity;
                            }
                        }
                    } elseif ($product->current_stock >= $request->quantity) {
                        if ($request->quantity >= $product->min_qty) {
                            $object['quantity'] = $request->quantity;
                        }
                    }
                }
                return $object;
            });
            $request->session()->put('cart', $cart);
        }

        $carts = getCarts();
        return view('frontend.partials.cart_details', compact('carts'));
    }
}
