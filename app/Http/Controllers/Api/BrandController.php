<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\BrandCollection;
use App\Models\Brand;
use App\Models\BusinessSetting;

class BrandController extends Controller
{
    public function index()
    {
        return new BrandCollection(brandTrans(Brand::all()));
    }

    public function top($limit = 8)
    {
        $homepageTop10 = BusinessSetting::where('type', 'top10_brands')->first();
        $homepageTop10 = json_decode($homepageTop10->value);
        return new BrandCollection(brandTrans(Brand::whereIn('id', $homepageTop10)->take($limit)->get()));
    }
}
