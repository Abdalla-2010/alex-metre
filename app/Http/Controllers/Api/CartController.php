<?php

namespace App\Http\Controllers\Api;

use App\Color;
use App\FlashDeal;
use App\Http\Resources\CartCollection;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{

    private $user_id = null;

    public function __construct(Request $request)
    {
        if (Auth::guard("api")->user()->token()) {
            $this->user_id = Auth::guard("api")->user()->token()->user_id;
        } else {
            return response()->json(["messaage" => "Unauthorized"], 401);
        }
    }

    public function index($id)
    {
        if ($this->user_id == $id) {
            return new CartCollection(getCarts($this->user_id));
        } else {
            return response()->json(["messaage" => "Something went wrong"], 404);
        }
    }

    public function add(Request $request)
    {
        $product = Product::findOrFail($request->product_id);
        $variant = $request->variant == null ? "" : $request->variant;
        $color = $request->color == null ? "" : $request->color;
        $tax = 0;
        $custom_size = null;
        $variation = "";


        if (($variant == '' && $color == '') || ($variant == null && $color == null))
            $price = $product->unit_price;
        else {
            if (($color != "" || $color != null) && !Color::where('code', $color)->first()) {
                return response()->json(notfound_response(['message' => translate("Can't found this product color!")]));
            } else {
                $variation = Color::where('code', $color)->first() ? Color::where('code', $color)->first()->name : "";
            }
            global $has_custom_size;
            $inputName = "custom_size_";
            if ($variant) {
                foreach ($variant as $v) {
                    if (strtolower($v['type']) == 'size' && str_contains($v['value'], $inputName)) {
                        $has_custom_size = true;
                        $custom_size = (int)$v['value'][strlen($inputName)];
                    }
                    if ($variation != null) {
                        $variation .= "-" . $v['value'];
                    } else {
                        $variation = $v['value'];
                    }
                }
            }
            if ($has_custom_size) {
                $price = $product->unit_price + $product->custom_size_price;
            } else {
                $product_stock = $product->stocks->where('variant', $variation)->first();
                if (!$product_stock) {
                    return response()->json(notfound_response(['message' => translate("Can't found this product attributes!")]));
                }
                $price = $product_stock->price;
            }
        }

        $flash_deals = FlashDeal::where('status', 1)->get();
        $inFlashDeal = false;
        foreach ($flash_deals as $flash_deal) {
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first() != null) {
                $flash_deal_product = FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first();
                if ($flash_deal_product->discount_type == 'percent') {
                    $price -= ($price * $flash_deal_product->discount) / 100;
                } elseif ($flash_deal_product->discount_type == 'amount') {
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }
        if (!$inFlashDeal) {
            if ($product->discount_type == 'percent') {
                $price -= ($price * $product->discount) / 100;
            } elseif ($product->discount_type == 'amount') {
                $price -= $product->discount;
            }
        }

        if ($product->tax_type == 'percent') {
            $tax = ($price * $product->tax) / 100;
        } elseif ($product->tax_type == 'amount') {
            $tax = $product->tax;
        }

        $oldCart = [
            'user_id' => $this->user_id,
            'product_id' => $request->product_id,
            'variation' => $variation
        ];

        $quantity = $request->quantity;

        $newCart = [
            'tax' => $tax,
            'shipping_cost' => 0,
        ];

        $quantity != null ? $newCart['quantity'] = DB::raw('quantity + ' . $quantity) : $newCart['quantity'] = DB::raw('quantity +  1');
        $custom_size != null ? $newCart['size_id'] = $custom_size : null;

        Cart::updateOrCreate($oldCart, $newCart);

        return response()->json([
            'message' => 'Product added to cart successfully'
        ]);
    }

    public function changeQuantity(Request $request)
    {
        $cart = Cart::find($request->id);
        if ($cart != null) {
            if ($cart->product->stocks->where('variant', $cart->variation)->first()->qty >= $request->quantity) {
                $cart->update([
                    'quantity' => $request->quantity
                ]);
                return response()->json(['message' => 'Cart updated', "status" => "success"], 200);
            } else {
                return response()->json(['message' => 'Maximum available quantity reached', "status" => "error"], 200);
            }
        }
        return response()->json(['message' => 'Something went wrong', "status" => "error"], 200);
    }

    public function destroy($id)
    {
        Cart::destroy($id);
        return response()->json(['message' => 'Product is successfully removed from your cart'], 200);
    }
}
