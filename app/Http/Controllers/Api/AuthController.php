<?php /** @noinspection PhpUndefinedClassInspection */

namespace App\Http\Controllers\Api;

use App\Models\BusinessSetting;
use App\Models\Customer;
use App\Notifications\EmailVerificationNotificationWithApi;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|str ing|max:255',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required'
        ]);
    }

    protected function create(array $data)
    {
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => '+' . $data['country_code'] . $data['phone'],
                'password' => Hash::make($data['password']),
            ]);

//            if (BusinessSetting::where('type', 'email_verification')->first()->value != 1) {
//                $user->email_verified_at = date('Y-m-d H:m:s');
//            } else {
//                $user->notify(new EmailVerificationNotificationWithApi());
//            }

            $user->notify(new EmailVerificationNotificationWithApi());

            $customer = new \App\Customer;
            $customer->user_id = $user->id;
            $customer->save();
        } else {
            if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated) {
                $user = User::create([
                    'name' => $data['name'],
                    'phone' => '+' . $data['country_code'] . $data['phone'],
                    'password' => Hash::make($data['password']),
                    'verification_code' => rand(100000, 999999)
                ]);

                $customer = new Customer;
                $customer->user_id = $user->id;
                $customer->save();
                $otpController = new OTPVerificationController;
                $otpController->send_code($user);
            }
        }

        return $user;
    }

    public
    function signup(Request $request)
    {
        if (!isset($request['email'])) $request['email'] = "";
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            if (User::where('email', $request->email)->first() != null) {
                return response()->json([
                    'message' => 'Email is exist',
                    'status' => 'error'
                ], 403);
            }
        } elseif (User::where('phone', '+' . $request->country_code . $request->phone)->first() != null) {
            return response()->json([
                'message' => 'Phone is exist',
                'status' => 'error'
            ], 403);
        }

        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        return response()->json([
            'message' => 'Registration Successful. Please verify and log in to your account.',
            'status' => 'success'
        ], 201);
    }

    public
    function login(Request $request)
    {
        $request->validate([
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $user = null;
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials))
                return response()->json(['message' => 'Unauthorized', 'user' => null], 401);
            $user = $request->user();
        } else {
            $userPhone = User::where('phone', '+' . $request->country_code . $request->phone)->first();
            if ($userPhone != null) {
                if (!Hash::check($request->password, $userPhone->password))
                    return response()->json(['message' => 'Unauthorized', 'user' => null], 401);
            }
            $user = $userPhone;
        }
        if ($user->email_verified_at == null) {
//            return response()->json(['message' => 'Please verify your account', 'user' => null], 401);
        }
        $tokenResult = $user->createToken('Personal Access Token');
        return $this->loginSuccess($tokenResult, $user);
    }

    public
    function user(Request $request)
    {
        return response()->json($request->user());
    }

    public
    function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public
    function socialLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email'
        ]);
        if (User::where('email', $request->email)->first() != null) {
            $user = User::where('email', $request->email)->first();
        } else {
            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'provider_id' => $request->provider,
                'email_verified_at' => Carbon::now()
            ]);
            $user->save();
            $customer = new Customer;
            $customer->user_id = $user->id;
            $customer->save();
        }
        $tokenResult = $user->createToken('Personal Access Token');
        return $this->loginSuccess($tokenResult, $user);
    }

    protected
    function loginSuccess($tokenResult, $user)
    {
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(100);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user' => [
                'id' => $user->id,
                'type' => $user->user_type,
                'name' => $user->name,
                'email' => $user->email,
                'avatar' => $user->avatar,
                'avatar_original' => $user->avatar_original,
                'address' => $user->address,
                'country' => $user->country,
                'city' => $user->city,
                'postal_code' => $user->postal_code,
                'phone' => $user->phone
            ]
        ]);
    }
}
