<?php

namespace App\Http\Controllers\Api;

use App\Mail\SecondEmailVerifyMailManager;
use App\Models\PasswordReset;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class PasswordResetController extends Controller
{

    public function reset_password_with_code(Request $request)
    {
        if (PasswordReset::where(["email" => $request->email, "token" => $request->token])->first() != null && (($user = User::where("email", $request->email)->first()) != null)) {
            if ($request->password == $request->password_confirmation) {
                $user->password = Hash::make($request->password);
                $user->email_verified_at = date('Y-m-d h:m:s');
                $user->save();

                PasswordReset::where(["email" => $request->email, "token" => $request->token])->delete();

                event(new \Illuminate\Auth\Events\PasswordReset($user));
                auth()->login($user, true);

                return response()->json([
                    'message' => translate("Password updated successfully"),
                    'status' => 200,
                    'success' => true,
                ]);

            } else {
                return response()->json([
                    'message' => translate("Password and confirm password didn't match"),
                    'status' => 401,
                    'success' => false,
                ]);
            }
        } else {
            return response()->json([
                'message' => translate("Verification code mismatch"),
                'status' => 404,
                'success' => false,
            ]);
        }
    }


    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user)
            return response()->json([
                'success' => false,
                'message' => 'We can not find a user with that e-mail address'], 404);

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => rand(100000, 999999)
            ]
        );

        if ($user && $passwordReset) {
            $array['view'] = 'emails.verification';
            $array['from'] = env('MAIL_USERNAME');
            $array['subject'] = translate('Password Reset');
            $array['content'] = 'Verification Code is ' . $passwordReset->token;

            Mail::to($user->email)->queue(new SecondEmailVerifyMailManager($array));
        }

        return response()->json([
            'success' => true,
            'message' => 'Please check your email. We have e-mailed your password reset link'
        ], 200);
    }
}
