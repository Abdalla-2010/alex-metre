<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\SizesCollection;
use App\Models\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SizesController extends Controller
{
    public function sizes($id)
    {
        return new SizesCollection(Size::where('user_id', $id)->get());
    }

    public function createSize(Request $request)
    {
        $size = new Size();
        $size->user_id = Auth::user()->token()->user_id;
        $size->name = $request->name;
        $size->height = $request->height;
        $size->width = $request->width;
        $size->size = json_encode($request->size);
        $size->save();

        return response()->json([
            'message' => 'Size information has been added successfully'
        ]);
    }

    public function deleteSize(Request $request)
    {
        $size = Size::findOrFail($request->id);
        $size->delete();
        return response()->json([
            'message' => 'Size information has been added deleted'
        ]);
    }
}
