<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PurchaseHistoryCollection;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PurchaseHistoryController extends Controller
{

    private $user_id = null;

    public function __construct(Request $request)
    {
        if (Auth::guard("api")->user()->token()) {
            $this->user_id = Auth::guard("api")->user()->token()->user_id;
        } else {
            return response()->json(["messaage" => "Unauthorized"], 401);
        }
    }

    public function index($id = null)
    {
        return new PurchaseHistoryCollection(Order::where('user_id', $this->user_id)->with(["orderDetails" => function ($q) {
            $q->select(["order_id", "delivery_status"]);
        }])->latest()->get()->map(function ($item) {
            $item->orderDetails->makeHidden("order_id");
            $item['delivery_status'] = $item->orderDetails->first() ? $item->orderDetails->first()->delivery_status : "pending";
            unset($item["orderDetails"]);
            return $item;
        }));
    }
}
