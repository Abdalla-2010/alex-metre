<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\SupportTicketCollection;
use App\Mail\SupportMailManager;
use App\Ticket;
use App\TicketReply;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SupportTicketController extends Controller
{
    public function tickets()
    {
        $tickets = Ticket::where('user_id', auth('api')->user()->id)->with('ticketreplies')->orderBy('created_at', 'desc')->get();
        return SupportTicketCollection::make($tickets);
    }

    public function store(Request $request)
    {
        $ticket = new Ticket;
        $ticket->code = max(100000, (Ticket::latest()->first() != null ? Ticket::latest()->first()->code + 1 : 0)) . date('s');
        $ticket->user_id = auth('api')->user()->id;
        $ticket->subject = $request->subject;
        $ticket->details = $request->details;
        $ticket->files = $request->attachments;
        if ($ticket->save()) {
            $this->send_support_mail_to_admin($ticket);
            return response()->json(success_response(['message' => translate('Ticket has been sent successfully')]));
        } else {
            return response()->json(notfound_response(['message' => translate('Something went wrong')]));
        }
    }

    public function reply_store(Request $request)
    {
        $ticket_reply = new TicketReply;
        $ticket_reply->ticket_id = $request->ticket_id;
        $ticket_reply->user_id = auth('api')->user()->id;
        $ticket_reply->reply = $request->reply;
        $ticket_reply->files = $request->attachments;
        $ticket_reply->ticket->viewed = 0;
        $ticket_reply->ticket->status = 'pending';
        $ticket_reply->ticket->save();
        if ($ticket_reply->save()) {
            return response()->json(success_response(['message' => translate('Reply has been sent successfully')]));
        } else {
            return response()->json(notfound_response(['message' => translate('Something went wrong')]));
        }
    }

    public function send_support_mail_to_admin($ticket)
    {
        $array['view'] = 'emails.support';
        $array['subject'] = 'Support ticket Code is:- ' . $ticket->code;
        $array['from'] = env('MAIL_USERNAME');
        $array['content'] = 'Hi. A ticket has been created. Please check the ticket.';
        $array['link'] = route('support_ticket.admin_show', encrypt($ticket->id));
        $array['sender'] = $ticket->user->name;
        $array['details'] = $ticket->details;

        try {
            Mail::to(User::where('user_type', 'admin')->first()->email)->queue(new SupportMailManager($array));
        } catch (\Exception $e) {
        }
    }
}
