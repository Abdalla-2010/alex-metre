<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Seller;
use Illuminate\Http\Request;

class SellerController extends Controller
{

    public function all()
    {
        $sellers = Seller::where("verification_status", 1)->select(["id", "user_id"])->with(["user" => function ($q) {
            $q->select("id", "name", "avatar");
        }])->get();
        $sellers->makeHidden("user_id");
        return $sellers;
    }

    public function getBestSellerHadSell($limit = 8)
    {
        $allProduct = Product::select(['user_id', 'num_of_sale'])->where("added_by", "seller")->get();
        $topSellers = [];
        foreach ($allProduct as $product) { 
            isset($topSellers[$product['user_id']]) ?
                $topSellers[$product['user_id']] += $product['num_of_sale'] : $topSellers[$product['user_id']] = $product['num_of_sale'];
        }
        arsort($topSellers);
        $sellers = Seller::whereIn('user_id', array_keys($topSellers))->with(["user" => function ($q) {
            $q->select("id", "name", "avatar");
        }])->select(["id", "user_id"])->take($limit)->get();
        $sellers->makeHidden("user_id");
        return $sellers->transform(function ($seller) use ($topSellers) {
            $seller['number_of_sell'] = $topSellers[$seller->user_id];
            return $seller;
        });
    }
}
