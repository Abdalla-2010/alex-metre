<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PurchaseHistoryDetailCollection;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PurchaseHistoryDetailController extends Controller
{

    private $user_id = null;

    public function __construct(Request $request)
    {
        if (Auth::guard("api")->user()->token()) {
            $this->user_id = Auth::guard("api")->user()->token()->user_id;
        } else {
            return response()->json(["messaage" => "Unauthorized"], 401);
        }
    }

    public function index($id)
    {
        return new PurchaseHistoryDetailCollection(OrderDetail::where('order_id', $id)->get());
    }
}
