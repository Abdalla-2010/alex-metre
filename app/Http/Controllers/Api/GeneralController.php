<?php

namespace App\Http\Controllers\Api;

use App\Models\NotificationToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{
    public function page($page)
    {
        $pages = ['terms' => "terms_conditions_page", 'return-policy' => "return_policy_page", 'support-policy' => "support_policy_page", 'privacy-policy' => "privacy_policy_page"];
        if (in_array($page, array_keys($pages))) {
            $content = \App\Page::where('type', $pages[$page])->first();
            return response()->json([
                'data' => [
                    "content" => strip_tags($content->getTranslation("content", routeLang()))
                ],
                'status' => 200,
                'succss' => true,
            ]);
        } else {
            return response()->json([
                'message' => translate("This Page doesn't exists"),
                'status' => 404,
                'succss' => false,
            ]);
        }
    }

    public function checkCoupon(Request $request)
    {
        if ($request->coupon != null) {
            $check = check_coupon($request->coupon, apiUserID(), getCarts(apiUserID()));
            return response()->json($check);
        } else {
            return response()->json(notfound_response(["message" => translate("Please add coupon!")]));
        }
    }

    public function saveTokenFirebase(Request $request)
    {
        $user_id = null;
        if ($request->user_id) {
            $user_id = $request->user_id;
        } else {
            $user_id = auth("api")->user()->id;
        }

        if ($user_id && $request->user_token) {
            NotificationToken::updateOrCreate(['user_id' => $user_id], ['user_token' => $request->user_token]);
            return response()->json(success_response(["message" => translate("Token added successfully.")]));
        } else {
            return response()->json(notfound_response(["message" => translate("We need user id and user token!")]));
        }
    }

    public function scan()
    {
        return response()->json([
            'lines' => 6
        ]);
    }
}
