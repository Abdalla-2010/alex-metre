<?php

namespace App\Http\Controllers\Api;

use App\Address;
use App\City;
use App\CityTranslation;
use App\Http\Resources\AddressCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{
    public function addresses($id)
    {
        $addresses = Address::where('user_id', $id)->get();
        $addresses->map(function ($address) {
            $city = City::find($address->city);
            if ($city) {
                $address->cost = $city->cost;
            } else {
                $address->cost = City::where('name', strtolower($address->city))->first() ? City::where('name', strtolower($address->city))->first()->cost : null;
                if ($address->cost == null) {
                    $cityTrans = CityTranslation::where('name', strtolower($address->city))->first();
                    if ($cityTrans) {
                        $address->cost = City::find($cityTrans->id)->cost;
                    }
                }
            }
            return $address;
        });
        return new AddressCollection($addresses);
    }

    public function createShippingAddress(Request $request)
    {
        $address = new Address;
        $address->user_id = Auth::user()->token()->user_id;
        $address->address = $request->address;
        $address->city = $request->city;
        //        $address->postal_code = $request->postal_code;
        $address->postal_code = "";
        $address->phone = $request->phone;
        $address->set_default = $request->set_default;
        $address->save();

        return response()->json([
            'message' => 'Shipping information has been added successfully'
        ]);
    }

    public function deleteShippingAddress(Request $request)
    {
        $address = Address::findOrFail($request->id);
        $address->delete();
        return response()->json([
            'message' => 'Shipping information has been deleted'
        ]);
    }
}
