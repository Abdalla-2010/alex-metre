<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\OTPVerificationController;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;

class VerificationController extends Controller
{

    use VerifiesEmails;

    protected $redirectTo = '/';

    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:2,1')->only('verify', 'resend');
    }

    public function show(Request $request)
    {
        if ($request->user()->email != null) {
            return $request->user()->hasVerifiedEmail()
                ? redirect($this->redirectPath())
                : view('auth.verify');
        } else {
            $otpController = new OTPVerificationController;
            $otpController->send_code($request->user());
            return redirect()->route('verification');
        }
    }

    public function resend(Request $request)
    {
        $user = User::where("email", $request->email)->first();
        if ($user) {
            if ($user->hasVerifiedEmail()) {
                return redirect($this->redirectPath());
            }
            $user->sendEmailVerificationNotificationWithApi();
            return response()->json(['status' => 'success', 'message' => translate("Resent verify email success, Please verify and log in to your account.")]);
        } else {
            return response()->json(['status' => 'error', 'message' => translate("This email doesn't exists")]);
        }
    }

    public function verification_confirmation($code)
    {
        $user = User::where('verification_code', $code)->first();
        if ($user != null) {
            $user->email_verified_at = Carbon::now();
            $user->verification_code = null;
            $user->save();
            auth()->login($user, true);
            return response()->json(['status' => 'success', 'message' => translate("Verify success, and you login now.")]);
        } else {
            return response()->json(['status' => 'error', 'message' => translate("Activate failed, Try again")]);
        }
    }
}
