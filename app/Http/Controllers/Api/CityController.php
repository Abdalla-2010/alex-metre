<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Country;
use App\Http\Resources\CityCollection;

class CityController extends Controller
{
    public function index()
    {
        return new CityCollection(City::all());
    }

    public function country($country = "Egypt")
    {
        $country = country::where("name", $country)->first();
        return new CityCollection(City::where('country_id', $country->id)->get());
    }
}
