<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProfileCollection;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index()
    {
        return ProfileCollection::make(User::where('id', auth('api')->user()->id)->with('addresses')->get());
    }

    public function update(Request $request)
    {
        $user = User::where("id", auth('api')->user()->id)->first();
        $user->name = $request->name;
        $user->address = $request->address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->postal_code = $request->postal_code;
        $user->phone = $request->phone;
        $password = $request->password;
        if ($password) {
            $validator = Validator::make($request->all(), [
                'password' => 'required|min:8',
                'password_confirm' => 'required|same:password'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            }
            $user->password = Hash::make($request->password);
        }
        $user->save();
        return response()->json([
            'user' => ProfileCollection::make(User::where('id', auth('api')->user()->id)->with('addresses')->get()),
            'message' => 'Profile information has been updated successfully',
            'success' => true,
            'status' => 200
        ]);
    }
}
