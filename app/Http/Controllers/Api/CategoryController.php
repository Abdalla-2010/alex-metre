<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CategoryCollection;
use App\Models\BusinessSetting;
use App\Models\Category;

class CategoryController extends Controller
{

    public function index()
    {
        return new CategoryCollection(categoryTrans(Category::where('level', 0)->get()));
    }

    public function featured($limit = 8)
    {
        return new CategoryCollection(categoryTrans(Category::where('featured', 1)->take($limit)->get()));
    }

    public function home()
    {
        $homepageCategories = BusinessSetting::where('type', 'home_categories')->first();
        $homepageCategories = json_decode($homepageCategories->value);
        return new CategoryCollection(categoryTrans(Category::whereIn('id', $homepageCategories)->get()));
    }

    public function top10($limit = 8)
    {
        $homepageTop10 = BusinessSetting::where('type', 'top10_categories')->first();
        $homepageTop10 = json_decode($homepageTop10->value);

        return new CategoryCollection(categoryTrans(Category::whereIn('id', $homepageTop10)->take($limit)->get()));
    }
}
