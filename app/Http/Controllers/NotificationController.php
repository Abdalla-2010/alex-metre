<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\NotificationToken;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = Notification::orderBy('created_at', 'desc')->paginate(15);
        return view('backend.notification.index', compact("notifications"));
    }

    public function store(Request $request)
    {
        $notification = new Notification;
        $notification->title = $request->notification_title;
        $notification->body = $request->notification_body;
        $notification->image = url("/public/" . api_asset($request->notification_image));
        if ($notification->save()) {

            $tokens = NotificationToken::whereNotNull('user_token')->pluck('user_token')->toArray();

            $SERVER_API_KEY = 'AAAADyDJu1c:APA91bEG0Bv7Q0sxBCd0Joep-fzQ10pfEHubo9_HfUXy4UjOY46aQiGyIku11PZnJz84AMpDZvm7HY92I2-LzEFpynihJFGmW_hQf_kvuoVukO3jdtiW5LNMqqSURacEm4231QGWWkDa
';

            $data = [
                "registration_ids" => $tokens,
                "notification" => [
                    "title" => $request->notification_title,
                    "body" => $request->notification_body,
                ]
            ];
            $dataString = json_encode($data);

            $headers = [
                'Authorization: key=' . $SERVER_API_KEY,
                'Content-Type: application/json',
            ];

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

            $response = curl_exec($ch);

//            $optionBuilder = new OptionsBuilder();
//            $optionBuilder->setTimeToLive(60 * 20);
//
//            $notificationBuilder = new PayloadNotificationBuilder($request->title);
//            $notificationBuilder->setBody($request->body)
//                ->setIcon($notification->image)
//                ->setSound('default');
//
//            $dataBuilder = new PayloadDataBuilder();
////            $dataBuilder->addData(['a_data' => 'my_data']);
//
//            $option = $optionBuilder->build();
//            $notification = $notificationBuilder->build();
//            $data = $dataBuilder->build();
//
//            $tokens = NotificationToken::pluck('user_token')->toArray();
//
//            try {
//                $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
//            } catch (\Exception $e) {
//                flash(translate('When send notification error happened!'))->error();
//                return redirect()->route("notification.index");
//            }
//
//            $downstreamResponse->numberSuccess();
//            $downstreamResponse->numberFailure();
//            $downstreamResponse->numberModification();
            flash(translate('Notifications had been sent successfully'))->success();
            return redirect()->route("notification.index");
        }
    }
}
