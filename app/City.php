<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class City extends Model
{
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getTranslation($field = '', $lang = false)
    {
        $lang = $lang == false ? App::getLocale() : $lang;
        $city_translation = $this->hasMany(CityTranslation::class)->where('lang', $lang)->first();
        return $city_translation != null ? $city_translation->$field : $this->$field;
    }

    public function allTrans($cities = null)
    {
        $cities = $cities == null ? $this->all() : $cities;
        $citiesTrans = CityTranslation::where("lang", routeLang())->get();
        $cities = $cities->map(function ($city) use ($citiesTrans) {
            $cityTrans = $citiesTrans->where("city_id", $city->id)->first();
            if ($cityTrans == null) return $city;
            $city['name'] = $cityTrans->name;
            return $city;
        });
        return $cities;
    }

    public function city_translations()
    {
        return $this->hasMany(CityTranslation::class);
    }
}
