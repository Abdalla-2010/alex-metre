<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Brand extends Model
{
    public function getTranslation($field = '', $lang = false)
    {
        $lang = $lang == false ? App::getLocale() : $lang;
        $brand_translation = $this->hasMany(BrandTranslation::class)->where('lang', $lang)->first();
        return $brand_translation != null ? $brand_translation->$field : $this->$field;
    }

    public function brand_translations()
    {
        return $this->hasMany(BrandTranslation::class);
    }

    public function allTrans($brands = null)
    {
        $brands = $brands == null ? $this->all() : $brands;
        $brandsTrans = BrandTranslation::where("lang", routeLang())->get();
        $brands = $brands->map(function ($brand) use ($brandsTrans) {
            $brandTrans = $brandsTrans->where("brand_id", $brand->id)->first();
            if ($brandTrans == null) return $brand;
            $brand['name'] = $brandTrans->name;
            return $brand;
        });
        return $brands;
    }

}
