@extends('backend.layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h1 class="mb-0 h6">{{translate('Notification Manager')}}</h1>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{ route('notification.store') }}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label">{{translate('Notification Title')}}</label>
                            <div class="col-sm-9">
                                <input type="text" name="notification_title" class="form-control"
                                       placeholder="{{translate('Notification Title')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label">{{translate('Notification Body')}}</label>
                            <div class="col-sm-9">
                                <input type="text" name="notification_body" class="form-control"
                                       placeholder="{{translate('Notification Body')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label">{{translate('Notification Image')}}</label>
                            <div class="col-sm-9">
                                <div class="input-group" data-toggle="aizuploader" data-type="image">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-soft-secondary">{{ translate('Browse') }}</div>
                                    </div>
                                    <div class="form-control file-amount">{{ translate('Choose Files') }}</div>
                                    <input type="hidden" name="types[]" value="notification_image">
                                    <input type="hidden" name="notification_image"
                                           class="selected-files">
                                </div>
                                <div class="file-preview box sm"></div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">{{ translate('Send to All') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0 h6">{{translate('All Notifications')}}</h5>
                </div>
                <div class="card-body">
                    <table class="table aiz-table mb-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{translate('Title')}}</th>
                            <th>{{translate('Body')}}</th>
                            <th>{{translate('Created At')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($notifications as $key => $notification)
                            <tr>
                                <td>{{ ($key+1) + ($notifications->currentPage() - 1)*$notifications->perPage() }}</td>
                                <td>{{ $notification->title }}</td>
                                <td>{{ $notification->body }}</td>
                                <td>{{ date('d-m-Y h:i:s a', strtotime($notification->created_at)) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--                    <div class="clearfix">--}}
                    {{--                        <div class="pull-right">--}}
                    {{--                            {{ $subscribers->appends(request()->input())->links() }}--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>

@endsection
