<div class="modal-body">
    <div class="text-center">
        <h1 class="h5 mb-1">{{ ucfirst($size->name) }}</h1>
        <p class="text-sm text-muted">{{ translate("Username") . ": " . $size->user->name }}</p>
    </div>

    <hr>
    <br>
    @if($size->size || $size->height || $size->width )
        <div class="table-responsive">
            <table class="table table-striped mar-no">
                <tbody>
                @if($size->height)
                    <tr>
                        <td>{{ translate("Height") }}</td>
                        <td>{{ $size->height }}</td>
                    </tr>
                @endif
                @if($size->width)
                    <tr>
                        <td>{{ translate("Width") }}</td>
                        <td>{{ $size->width }}</td>
                    </tr>
                @endif
                @if($size->size)
                    @php
                        //generatePageTree(json_decode($size->size));
                    @endphp
                @endif
                </tbody>
            </table>
            @if($size->size)
                <div class="p-5">
                    @php
                        $array = new \App\Http\Controllers\ArrayToPrint();
                        $array->display_hierarchy(json_decode($size->size));
                    @endphp
                </div>
            @endif
        </div>
    @endif
</div>
