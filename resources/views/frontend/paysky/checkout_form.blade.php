@extends('frontend.layouts.app')
@section('script')
    <script src="https://grey.paysky.io:9006/invchost/JS/LightBox.js?v=1.1"></script>
    <script type="text/javascript">
        function callLightbox() {
            let mID = "{{env("PAYSKY_MID")}}"
            let tID = "{{env("PAYSKY_TID")}}"
            if (mID === '' || tID === '') {
                return;
            }
            Lightbox.Checkout.configure = {
                MID: mID,
                TID: tID,
                AmountTrxn: {{(int) $order->grand_total * 100}},
                MerchantReference: "Txn-1234",
                TrxDateTime: '202009171418',
                SecureHash: 'EAD7AB68E23BFF2E5B03F4A0CD41581722FD14C349C6743CD91B577341465A61',
                completeCallback: function (data) {
                    $.ajax({
                        url: "{{route('paysky.checkout.return')}}",
                        type: "POST",
                        data: {"data": data, "_token": "{{ csrf_token() }}"},
                        success: function (DD) {
                            location.href = DD.to;
                        },
                    });
                },
                errorCallback: function (data) {
                    console.log('error');
                    console.log(data);
                },
                cancelCallback: function () {
                    console.log('cancel');
                }
            };

            Lightbox.Checkout.showLightbox();
        }

        callLightbox()
    </script>
@endsection
