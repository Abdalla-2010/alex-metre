<style>
    .modal.show .modal-dialog.modal-dialog-zoom {
        max-width: 900px;
    }

    .modal-content .modal-body {
        overflow-y: hidden;
    }

    .go-to-app {
        display: flex;
        flex-direction: row;
        align-items: center;
    }

    .go-to-app span {
        color: black;
    }
</style>
<div class="modal-body p-4 added-to-cart">
    <div class="text-center text-success mb-4">
        <h3 class="title-choose-product-size">{{ translate('Choose Product Size')}}</h3>
        <h3 class="title-go-to-our-app d-none">{{ translate('Go to our app')}}</h3>
    </div>
    <div class="media mb-4 sizes-body-list justify-content-center">
        @if($userSizes->count())
            <form class="form-default form-save-sizes" data-toggle="validator"
                  role="form" method="POST">
                @csrf
                @if(Auth::check())
                    <div class="col-md-6 mx-auto mb-3">
                        <div
                            class="border p-3 rounded mb-3 c-pointer text-center bg-white h-100 d-flex flex-column justify-content-center"
                            onclick="add_new_size()">
                            <i class="las la-plus la-2x mb-3"></i>
                            <div class="alpha-7">{{ translate('Add New Size') }}</div>
                        </div>
                    </div>
                    <div class="shadow-sm bg-white rounded mb-4"
                         style="max-height: 13vh; overflow: hidden;overflow-y: auto">
                        <div class="row gutters-5" style="padding: 0 30px;">
                            @foreach ($userSizes as $key => $size)
                                <div class="col-md-6 mb-2" style="height: auto">
                                    <label class="aiz-megabox d-block bg-white mb-0">
                                        <input type="radio" name="size_id" value="{{ $size->id }}"
                                               @if ($size->id == $userSizes->first()->id)
                                               checked
                                               @endif required>
                                        <span class="d-flex p-3 aiz-megabox-elem">
                                                <span class="aiz-rounded-check flex-shrink-0 mt-1"></span>
                                                <span class="flex-grow-1 pl-3 text-left">
                                                    <div>
                                                        <span class="opacity-60">{{ translate('Size Name') }}:</span>
                                                        <span class="fw-600 ml-2">{{ $size->name }}</span>
                                                    </div>
                                                    <div>
                                                        <span class="opacity-60">{{ translate('Height') }}:</span>
                                                        <span class="fw-600 ml-2">{{ $size->height }}</span>
                                                    </div>
                                                    <div>
                                                        <span class="opacity-60">{{ translate('Width') }}:</span>
                                                        <span class="fw-600 ml-2">{{ $size->width }}</span>
                                                    </div>
                                                </span>
                                            </span>
                                    </label>
                                </div>
                            @endforeach
                            <input type="hidden" name="checkout_type" value="logged">
                        </div>
                    </div>
                @else

                @endif
            </form>
        @else
            <form class="form-default w-100" data-toggle="validator"
                  action="{{ route('checkout.store_shipping_infostore') }}"
                  role="form" method="POST">
                @csrf
                <div class="col-md-6 mx-auto mb-3">
                    <div
                        class="border p-3 rounded mb-3 c-pointer text-center bg-white h-100 d-flex flex-column justify-content-center"
                        onclick="add_new_size()">
                        <i class="las la-plus la-2x mb-3"></i>
                        <div class="alpha-7">{{ translate('Add New Size') }}</div>
                    </div>
                </div>

                <div class="text-center">
                    {{translate("There's no sizes for you.")}}
                </div>

            </form>
        @endif
    </div>
    <div class="media mb-4 sizes-body-add d-none">
        <div class="text-center m-auto">
            <div class="qr-code p-1 border float-right ml-4">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" baseProfile="tiny" width="200" height="200">
                    <rect shape-rendering="optimizeSpeed" x="0" y="0" width="200" height="200" fill="white"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="42" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="17" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="22" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="27" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="32" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="37" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="42" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="42" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="47" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="52" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="52" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="57" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="52" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="62" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="67" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="52" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="72" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="77" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="82" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="42" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="87" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="52" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="92" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="52" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="97" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="52" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="102" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="107" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="42" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="112" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="42" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="117" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="52" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="122" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="127" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="42" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="52" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="132" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="137" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="142" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="42" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="147" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="82" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="152" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="157" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="112" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="157" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="177" y="162" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="77" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="92" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="167" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="67" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="87" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="97" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="117" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="122" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="142" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="162" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="167" y="172" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="17" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="22" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="27" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="32" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="37" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="42" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="47" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="57" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="62" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="72" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="102" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="107" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="127" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="132" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="137" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="147" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="152" y="177" width="5" height="5" fill="black"/>
                    <rect shape-rendering="optimizeSpeed" x="172" y="177" width="5" height="5" fill="black"/>
                </svg>
            </div>
            <div class="go-to-app float-left p-2 border h-210px w-200px">
                @php
                    $logo = get_setting('header_logohttps://alexmetre.com/track_your_order');
                @endphp
                <a href="https://play.google.com/store/apps/details?id=flyiv.com" target="_blank"
                   class="d-block m-auto text-center">
                    @if($logo != null)
                        <img loading="lazy" class="d-block" src="{{ uploaded_asset($logo) }}" height="40"
                             style="display:inline-block;">
                    @else
                        <img loading="lazy" class="d-block m-auto"
                             src="{{ static_asset('uploads/all/Google-play-icon.png') }}"
                             height="50" style="display:inline-block;">
                    @endif
                    <span class="mt-3 d-block">{{translate("Go to our app")}}</span>
                </a>
            </div>
        </div>
    </div>
    <div class="text-center sizes-body-save sizes-body-add-to-cart">
        <button class="btn btn-outline-primary mb-3 mb-sm-0"
                data-dismiss="modal">{{ translate('Save')}}</button>
    </div>
</div>

<script>
    $(".sizes-body-save").click(function () {
        $.ajax({
            type: "POST",
            url: '{{ route('save-size-id') }}',
            data: $(".form-save-sizes").serializeArray(),
            success: function (data) {
                if (data.status) {
                    $(".custom-size-label").text(`${data.size.height}×${data.size.width} | ${data.size.name}`)
                    $(".custom-size-input").attr("value", `custom_size_${data.size.id}`)
                    AIZ.plugins.notify('success', "{{ translate('Yout select custom size with name ') }} " + `${data.size.name} - ${data.size.height}×${data.size.width}`);
                    // customSizeChecked(data)
                    getVariantPrice()
                } else {
                    AIZ.plugins.notify('danger', "{{ translate('There is no product can print on, try again later') }}");
                }
            }
        });
    })
</script>
