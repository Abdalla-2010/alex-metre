<div class="card border-0 shadow-sm rounded">
    <div class="card-header">
        <h3 class="fs-16 fw-600 mb-0">{{translate('Summary')}}</h3>
        <div class="text-right">
            {{--            <span--}}
            {{--                class="badge badge-inline badge-primary">{{ count(Session::get('cart_' . Session::get('owner_id'))) }} {{translate('Items')}}</span>--}}
            <span
                class="badge badge-inline badge-primary">{{ count(Session::get('cart')) }} {{translate('Items')}}</span>
            {{--            <span class="badge badge-inline badge-primary">{{ count(Session::get('cart')->where('owner_id', Session::get('owner_id'))) }} {{translate('Items')}}</span>--}}
        </div>
    </div>

    <div class="card-body">
        @if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated)
            @php
                $total_point = 0;
//dd(Session::get('cart_' . Session::get('owner_id')));
            @endphp
            {{--            @foreach (Session::get('cart_' . Session::get('owner_id')) as $key => $cartItem)--}}
            @foreach (Session::get('cart') as $key => $cartItem)
                @php
                    $product = \App\Product::find($cartItem['id']);
                    $total_point += $product->earn_point*$cartItem['quantity'];
                @endphp
            @endforeach
            <div class="rounded px-2 mb-2 bg-soft-primary border-soft-primary border">
                {{ translate("Total Club point") }}:
                <span class="fw-700 float-right">{{ $total_point }}</span>
            </div>
        @endif
        <table class="table">
            <thead>
            <tr>
                <th class="product-name">{{translate('Product')}}</th>
                <th class="product-total text-right">{{translate('Total')}}</th>
            </tr>
            </thead>
            <tbody>
            @php
                $subtotal = 0;
                $tax = 0;
                $shipping = 0;
            @endphp
            @foreach (Session::get('cart') as $key => $cartItem)
                @php
                    $design = null;
                    $size = null;
                    $product = \App\Product::find($cartItem['product_id']);
                    $product_name_with_choice = $product->getTranslation('name');
                    if($cartItem['design_id'] != null){
                        $design = \App\Product::find($cartItem['design_id']);
                        $subtotal = $subtotal + $cartItem['price']*$cartItem['quantity'] + $design->unit_price ;
                    }else {
                        if($cartItem['size_id'] != null){
                        $size = \App\Models\Size::find($cartItem['size_id']);
                        $subtotal = $subtotal + ( $cartItem['price'])*$cartItem['quantity'];
                        $product_name_with_choice = $product->getTranslation('name').' | ' . translate("Size") . ' : '. $size->name;
                        }else {
                            $subtotal = $subtotal + $cartItem['price']*$cartItem['quantity'] ;
                        }
                    }
                    if ($cartItem['variation'] != null) {
                        if(str_contains($cartItem['variation'], "custom_size_")){
                            if(strpos($cartItem['variation'], "custom_size_") != 0) {
                                $color = substr($cartItem['variation'], 0,  strpos($cartItem['variation'], "custom_size_") -1 );
                                $product_name_with_choice = $product->getTranslation('name').' | ' . translate("Color") . ' : '.  $color .' | ' . translate("Size") . ' : '. $size->name;
                            }
                        }else {
                            $product_name_with_choice = $product->getTranslation('name').' | '.$cartItem['variation'];
                        }
                    }
                    if($cartItem['design_id'] != null){
                        $product_name_with_choice = $product->getTranslation('name').' | Design: '. $design->getTranslation("name");
                    }
                    $shipping += $cartItem['shipping'];
                @endphp
                <tr class="cart_item">
                    <td class="product-name">
                        {{ $product_name_with_choice }}
                        <strong class="product-quantity">× {{ $cartItem['quantity'] }}</strong>
                    </td>
                    <td class="product-total text-right">
                        <strong>
                            @if(!$design && !$size)
                                <span
                                    class="pl-4">{{ single_price(($cartItem['price']+$cartItem['tax'])*$cartItem['quantity']) }}</span>
                            @elseif($size)
                                <span
                                    class="pl-4">{{ single_price(($cartItem['price']+$cartItem['tax'] )*$cartItem['quantity']) }}</span>
                            @elseif($design)
                                <span
                                    class="pl-4">{{ single_price(($cartItem['price']+$cartItem['tax'])*$cartItem['quantity'] + $design->unit_price) }}</span>
                            @endif
                        </strong>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <table class="table">

            <tfoot>
            <tr class="cart-subtotal">
                <th>{{translate('Subtotal')}}</th>
                <td class="text-right">
                    <span class="fw-600">{{ single_price_space($subtotal) }}</span>
                </td>
            </tr>

            {{--            <tr class="cart-shipping">--}}
            {{--                <th>{{translate('Tax')}}</th>--}}
            {{--                <td class="text-right">--}}
            {{--                    <span class="font-italic">{{ single_price_space($tax) }}</span>--}}
            {{--                </td>--}}
            {{--            </tr>--}}

            <tr class="cart-shipping">
                <th>{{translate('Total Shipping')}}</th>
                <td class="text-right">
                    <strong><span>{{ single_price_space($shipping) }}</span></strong>
                </td>
            </tr>

            @if (Session::has('coupon_discount'))
                <tr class="cart-shipping">
                    <th>{{translate('Coupon Discount')}}</th>
                    <td class="text-right">
                        <strong><span>{{ single_price_space(Session::get('coupon_discount')) }}</span></strong>
                    </td>
                </tr>
            @endif

            @php
                $total = $subtotal+$tax+$shipping;
                if(Session::has('coupon_discount')){
                    $total -= Session::get('coupon_discount');
                }
            @endphp

            <tr class="cart-total">
                <th><span class="strong-600">{{translate('Total')}}</span></th>
                <td class="text-right">
                    <strong><span>{{ single_price_space($total) }}</span></strong>
                </td>
            </tr>
            </tfoot>
        </table>

        @if (Auth::check() && \App\BusinessSetting::where('type', 'coupon_system')->first()->value == 1)
            @if (Session::has('coupon_discount'))
                <div class="mt-3">
                    <form class="" action="{{ route('checkout.remove_coupon_code') }}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="input-group">
                            <div class="form-control">{{ \App\Coupon::find(Session::get('coupon_id'))->code }}</div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">{{translate('Change Coupon')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            @else
                <div class="mt-3">
                    <form class="" action="{{ route('checkout.apply_coupon_code') }}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="input-group">
                            <input type="text" class="form-control" name="code"
                                   placeholder="{{translate('Have coupon code? Enter here')}}" required>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">{{translate('Apply')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        @endif

    </div>
</div>
