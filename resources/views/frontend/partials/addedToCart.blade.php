<div class="modal-body p-4 added-to-cart">
    <div class="text-center text-success mb-4">
        <i class="las la-check-circle la-3x"></i>
        <h3>{{ translate('Item added to your cart!')}}</h3>
    </div>
    <div class="media mb-4">
        @php
            $cartItem = $data2;
            $product = \App\Product::find($cartItem['product_id']);
            $product_name_with_choice = $product->getTranslation('name');
            if ($cartItem['variation'] != null) {
              if(str_contains($cartItem['variation'], "custom_size_")){
                  if(strpos($cartItem['variation'], "custom_size_") != 0) {
                      $color = substr($cartItem['variation'], 0,  strpos($cartItem['variation'], "custom_size_") -1 );
                      $size_id = substr($cartItem['variation'], strpos($cartItem['variation'], "custom_size_") + 12);
                      $size = \App\Models\Size::find($size_id);
                      $product_name_with_choice = $product->getTranslation('name').' | ' . translate("Color") . ' : '.  $color .' | ' . translate("Size") . ' : '. $size->name;
                  }
              }else {
                  $product_name_with_choice = $product->getTranslation('name').' | '.$cartItem['variation'];
              }
            }
        @endphp
        @if(isset($design))
            <img src="{{ static_asset('assets/img/placeholder.jpg') }}"
                 data-src="{{ uploaded_asset($tshirt->thumbnail_img) }}"
                 class="mr-3 lazyload size-100px img-fit rounded"
                 alt="Product Image">
        @else
            <img src="{{ static_asset('assets/img/placeholder.jpg') }}"
                 data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                 class="mr-3 lazyload size-100px img-fit rounded"
                 alt="Product Image">
        @endif
        <div class="media-body pt-3 text-left">
            @if(isset($design))
                <h6 class="fw-600">
                    {{  $tshirt->getTranslation('name') . translate(" + Design: ") .  $design->getTranslation('name') }}
                </h6>
            @else
                <h6 class="fw-600">
                    {{  $product_name_with_choice  }}
                </h6>
            @endif
            <div class="row mt-3">
                @if(isset($design))
                    <div class="col-sm-2 opacity-60">
                        <div>{{ translate('Price')}}:</div>
                    </div>
                    <div class="col-sm-10">
                        <div class="h6 text-primary">
                            <strong>
                                {{ single_price(($data['price']+$data['tax'])*$data['quantity'] + $design->unit_price) }}
                            </strong>
                        </div>
                    </div>
                @else
                    <div class="col-sm-2 opacity-60">
                        <div>{{ translate('Price')}}:</div>
                    </div>
                    <div class="col-sm-10">
                        <div class="h6 text-primary">
                            <strong>
                                {{ single_price(($data['price']+$data['tax'])*$data['quantity']) }}
                            </strong>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="text-center">
        <button class="btn btn-outline-primary mb-3 mb-sm-0"
                data-dismiss="modal">{{ translate('Back to shopping')}}</button>
        <a href="{{ route('cart') }}" class="btn btn-primary mb-3 mb-sm-0">{{ translate('Proceed to Checkout')}}</a>
    </div>
</div>
