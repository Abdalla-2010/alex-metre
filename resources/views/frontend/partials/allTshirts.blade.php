<div class="modal-body p-4 added-to-cart">
    <div class="text-center text-success mb-4">
        <i class="las la-check-circle la-3x"></i>
        <h3>{{ translate('Select t-shirt want to print on..')}}</h3>
    </div>

    <form class="form-default d-none" role="form" id="addToCartDesignWithTshirt" method="POST">
        @csrf
        <input type="hidden" name="product_id" value="{{$product->id}}"/>
        <input type="hidden" name="tshirt_id" class="tshirt-input"/>
    </form>

    <div class="t-shirt-list">
        @foreach($productsTshirt as $product)
            <div class="media mb-4" data-id="{{$product->id}}">
                <img
                    class="mr-3 lazyload size-100px img-fit rounded"
                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                    data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                >
                <div class="media-body pt-3 text-left">
                    <h6 class="fw-600">
                        {{  $product->getTranslation('name')  }}
                    </h6>
                    <div class="row mt-3">
                        <div class="col-sm-2 opacity-60">
                            <div>{{ translate('Price')}}:</div>
                        </div>
                        <div class="col-sm-10">
                            <div class="h6 text-primary">
                                <strong>
                                    {{ single_price(($product->unit_price)) }}
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

<style>
    .t-shirt-list > div {
        cursor: pointer;
    }

    .t-shirt-list > div:hover {
        background: #dddddd;
    }
</style>

<script>
    $(".t-shirt-list > div").click(function () {
        $(".tshirt-input").attr("value", $(this).data("id"))
        $.ajax({
            type: "POST",
            url: '{{ route('cart.addToCartWithTshirt') }}',
            data: $("#addToCartDesignWithTshirt").serializeArray(),
            success: function (data) {
                console.log(data)
                if (data.status) {
                    if (data.buynow == 1) {
                        updateNavCart();
                        $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html()) + 1);
                        window.location.replace("{{ route('cart') }}");
                    } else {
                        $('#addToCart').modal();
                        $('.c-preloader').show();
                        $('#addToCart-modal-body').html(null);
                        $('.c-preloader').hide();
                        $('#modal-size').removeClass('modal-lg');
                        $('#addToCart-modal-body').html(data.view);
                        updateNavCart();
                        $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html()) + 1);
                    }
                } else {
                    AIZ.plugins.notify('danger', "{{ translate('There is no product can print on, try again later') }}");
                }
            }
        });
    })
</script>
